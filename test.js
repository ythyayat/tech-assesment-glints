const cek = string => {
  let temp = [];
  let tempIndex = 0;

  for (let index = 0; index < string.length; index++) {
    const element = string[index];

    if (element === '}' || element === ']' || element === ')') {
      if (tempIndex === 0) {
        return false;
      }

      switch (element) {
        case '}':
          if (temp[tempIndex - 1] === '{') {
            tempIndex--
          } else {
            return false
          }
          break;
        case ']':
          if (temp[tempIndex - 1] === '[') {
            tempIndex--
          } else {
            return false
          }
          break;
        case ')':
          if (temp[tempIndex - 1] === '(') {
            tempIndex--
          } else {
            return false
          }
          break;
      
        default:
          break;
      }

    }
    else if (element === '{' || element === '[' || element === '(') {
      temp[tempIndex] = element;
      tempIndex++;
    }
  }
  return true
}

// console.log(cek('()'));
// console.log(cek('({})'));
// console.log(cek('({a})'));
// console.log(cek('([)'));
// console.log(cek('([))'));
// console.log(cek(']([))'));
console.log(cek('{ab()[]}'));